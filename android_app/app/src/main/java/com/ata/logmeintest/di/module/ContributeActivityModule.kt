package com.ata.logmeintest.di.module

import com.ata.logmeintest.ui.main.MainActivity
import com.ata.logmeintest.di.scope.ActivityScope
import com.ata.logmeintest.ui.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ContributeActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributeMainActivityInjector(): MainActivity
}