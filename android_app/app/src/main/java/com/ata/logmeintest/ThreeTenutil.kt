package com.ata.logmeintest

import org.threeten.bp.*
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle

const val MINUTE = 60000

fun LocalDateTime.asMillis() = this.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()

fun Long.asLocalDateTime(): LocalDateTime =
    Instant.ofEpochMilli(this).atZone(ZoneId.systemDefault()).toLocalDateTime().withNano(0)

fun isoFormatToMillis(date: String): Long {
    val zdt = ZonedDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME)
    return zdt.toLocalDateTime().asMillis()
}

fun LocalDateTime.diffInMillis(localDateTime: LocalDateTime): Long =
    Duration.between(this, localDateTime).toMillis()
