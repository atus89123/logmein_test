package com.ata.logmeintest.ui.main

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.ata.logmeintest.ui.util.UiCommandSource

interface MainViewModel : UiCommandSource{

    val weatherUiData: ObservableField<Weather>

    val isRefreshing: ObservableBoolean

    fun fetchWeather()

    data class Weather(
        val name: String,
        val imageAbbr: String,
        val created: String,
        val temp: String
    )
}
