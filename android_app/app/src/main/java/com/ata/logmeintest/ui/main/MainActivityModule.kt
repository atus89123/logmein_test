package com.ata.logmeintest.ui.main

import com.ata.logmeintest.di.module.createOrReUseViewModel
import com.ata.logmeintest.di.scope.ActivityScope
import dagger.Module
import dagger.Provides
import javax.inject.Provider

@Module
class MainActivityModule {

    @ActivityScope
    @Provides
    fun provideViewModel(
        activity: MainActivity,
        provider: Provider<MainViewModelImpl>
    ): MainViewModel {
        return createOrReUseViewModel(activity, provider)
    }
}