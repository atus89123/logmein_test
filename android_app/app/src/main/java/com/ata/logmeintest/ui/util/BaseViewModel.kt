package com.ata.logmeintest.ui.util

import androidx.lifecycle.ViewModel
import com.afollestad.materialdialogs.MaterialDialog
//import com.afollestad.materialdialogs.MaterialDialog
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


abstract class BaseViewModel(
    private val uiCommandSource: ExecuteOnceUiCommandSource
): RxViewModel(), UiCommandSource by uiCommandSource {

    fun setUiCommand(command: UiCommand) {
        uiCommandSource.uiCommand.value = command
    }

    fun showMessage(message: String) = setUiCommand {
        MaterialDialog(this).show {
            message(text = message)
            positiveButton(android.R.string.ok)
        }
    }
}

/**
 * Inherit from RxViewModel and use disposeOnCleared to prevent memory leak when use rx operations
 */
open class RxViewModel : ViewModel() {

    private val disposables = CompositeDisposable()

    fun Disposable.disposeOnCleared() = disposables.add(this)

    public override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}