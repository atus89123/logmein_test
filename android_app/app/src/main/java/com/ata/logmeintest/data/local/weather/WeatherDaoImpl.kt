package com.ata.logmeintest.data.local.weather

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
abstract class WeatherDaoImpl : WeatherDao {

    @Query("SELECT * FROM weather")
    abstract override fun getWeatherOrEmpty(): Single<WeatherEntity>

    @Query("SELECT * FROM weather")
    abstract override fun getWeather(): Observable<WeatherEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract override fun saveWeather(weather: WeatherEntity): Completable
}