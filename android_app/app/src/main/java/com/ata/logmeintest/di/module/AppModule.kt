package com.ata.logmeintest.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import com.ata.logmeintest.LogMeInApplication
import com.ata.logmeintest.data.local.AppDatabase
import com.ata.logmeintest.data.local.weather.WeatherDao
import com.ata.logmeintest.data.model.WeatherRepository
import com.ata.logmeintest.data.model.WeatherRepositoryImpl
import com.ata.logmeintest.data.remote.WeatherApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

const val API_URL = "https://www.metaweather.com/api/"

@Module(includes = [AppModule.DelegateBindings::class])
class AppModule {

    @Module
    interface DelegateBindings {
        @Binds
        @Singleton
        fun provideApplication(delegate: LogMeInApplication): Application

        @Binds
        @Singleton
        fun provideContext(delegate: Application): Context

        @Binds
        @Singleton
        fun provideWeatherRepository(delegate: WeatherRepositoryImpl): WeatherRepository
    }

    @Provides
    @Singleton
    fun provideObjectMapper(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    internal fun provideOkhttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        return httpClient.build()
    }

    @Provides
    @Named(API_URL)
    fun provideEndPointUrl() = API_URL

    @Provides
    @Singleton
    internal fun provideWeatherApi(
        @Named(API_URL) url: String,
        objectMapper: Gson,
        okHttpClient: OkHttpClient): WeatherApi {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(objectMapper))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .baseUrl(url)
            .client(okHttpClient)
            .build()
            .create(WeatherApi::class.java)
    }

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context) =
        Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "database"
        )
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideCartDao(db: AppDatabase): WeatherDao = db.getWeatherDao()
}