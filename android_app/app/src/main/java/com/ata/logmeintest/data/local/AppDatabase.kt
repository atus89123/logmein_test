package com.ata.logmeintest.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ata.logmeintest.data.local.weather.WeatherDaoImpl
import com.ata.logmeintest.data.local.weather.WeatherEntity

@Database(
    entities = [WeatherEntity::class],
    version = 2,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getWeatherDao(): WeatherDaoImpl
}