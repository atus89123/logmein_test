package com.ata.logmeintest.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.ata.logmeintest.R
import com.ata.logmeintest.databinding.ActivityMainBindingImpl
import com.ata.logmeintest.ui.util.observeAndRunUiCommands
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as? HasAndroidInjector)?.androidInjector()?.inject(this)
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<ActivityMainBindingImpl>(this, R.layout.activity_main).apply {
            lifecycleOwner = this@MainActivity
            viewModel = this@MainActivity.viewModel
        }

        observeAndRunUiCommands(viewModel.uiCommand)

        swipeRefresh.setOnRefreshListener {
            viewModel.fetchWeather()
        }
    }
}