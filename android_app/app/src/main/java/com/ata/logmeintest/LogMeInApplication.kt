package com.ata.logmeintest

import com.ata.logmeintest.di.component.DaggerAppComponent
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class LogMeInApplication : DaggerApplication() {

    private val appComponent: AndroidInjector<LogMeInApplication> by lazy {
        DaggerAppComponent
            .builder()
            .create(this)
    }

    override fun applicationInjector() = appComponent

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
    }
}