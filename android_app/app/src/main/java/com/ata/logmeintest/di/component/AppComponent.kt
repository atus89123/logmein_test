package com.ata.logmeintest.di.component

import com.ata.logmeintest.LogMeInApplication
import com.ata.logmeintest.di.module.AppModule
import com.ata.logmeintest.di.module.ContributeActivityModule
import com.ata.logmeintest.di.module.ContributeFragmentModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ContributeActivityModule::class,
        ContributeFragmentModule::class
    ]
)
interface AppComponent : AndroidInjector<LogMeInApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<LogMeInApplication>()
}