package com.ata.logmeintest.data.local.weather

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "weather")
@Parcelize
data class WeatherEntity(
    @PrimaryKey val key: Long = 1,
    val savedTime: Long,
    val name: String,
    val imageAbbr: String,
    val created: Long,
    val temp: Float
): Parcelable