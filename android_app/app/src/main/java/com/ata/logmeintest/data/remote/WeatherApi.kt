package com.ata.logmeintest.data.remote

import com.ata.logmeintest.data.remote.dto.Weathers
import io.reactivex.Observable
import retrofit2.http.GET

interface WeatherApi {

    @GET("location/804365/")
    fun getWeathers(): Observable<Weathers>
}