package com.ata.logmeintest.data.local.weather

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface WeatherDao {
    fun getWeatherOrEmpty(): Single<WeatherEntity>
    fun getWeather(): Observable<WeatherEntity>
    fun saveWeather(weather: WeatherEntity): Completable
}