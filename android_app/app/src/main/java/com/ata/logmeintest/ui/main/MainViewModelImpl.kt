package com.ata.logmeintest.ui.main

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.ata.logmeintest.asLocalDateTime
import com.ata.logmeintest.data.model.WeatherRepository
import com.ata.logmeintest.ui.util.BaseViewModel
import com.ata.logmeintest.ui.util.ExecuteOnceUiCommandSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle

import javax.inject.Inject

class MainViewModelImpl @Inject constructor(
    uiCommandSource: ExecuteOnceUiCommandSource,
    private val weatherRepository: WeatherRepository
) : BaseViewModel(uiCommandSource), MainViewModel {

    override val weatherUiData: ObservableField<MainViewModel.Weather> = ObservableField()
    override val isRefreshing: ObservableBoolean = ObservableBoolean(false)

    init {
        fetchWeather()
    }

    private fun dateTimeFormat(date: Long): String =
        date.asLocalDateTime().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT))

    override fun fetchWeather() {
        weatherRepository.getCurrentWeather()
            .doOnSubscribe { isRefreshing.set(true) }
            .doOnNext { isRefreshing.set(false) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    weatherUiData.set(
                        MainViewModel.Weather(
                            name = it.name,
                            imageAbbr = it.imageAbbr,
                            created = dateTimeFormat(it.created),
                            temp = it.temp.toString()
                        )
                    )

                },
                onError = { showMessage(it.localizedMessage ?: "error") }
            ).disposeOnCleared()
    }
}