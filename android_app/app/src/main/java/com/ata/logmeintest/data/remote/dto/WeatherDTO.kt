package com.ata.logmeintest.data.remote.dto

import com.google.gson.annotations.SerializedName

data class Weathers(
    @SerializedName("consolidated_weather") val weathers: List<WeatherDTO>
)

data class WeatherDTO(
    @SerializedName("weather_state_name") val name: String,
    @SerializedName("weather_state_abbr") val imageAbbr: String,
    @SerializedName("created") val created: String,
    @SerializedName("the_temp") val temp: Float
)