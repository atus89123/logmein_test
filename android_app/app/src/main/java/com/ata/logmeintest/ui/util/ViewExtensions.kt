package com.ata.logmeintest.ui.util

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun FragmentActivity.observeAndRunUiCommands(commands: LiveData<UiCommand>) =
    observeAndRunUiCommands(commands) { this }

fun LifecycleOwner.observeAndRunUiCommands(
    commands: LiveData<UiCommand>,
    getActivity: () -> FragmentActivity
) =
    commands.observe(this, Observer { it?.invoke(getActivity()) })