package com.ata.logmeintest.data.model

import com.ata.logmeintest.data.local.weather.WeatherEntity
import io.reactivex.Completable
import io.reactivex.Observable

interface WeatherRepository {
    fun getCurrentWeather(): Observable<WeatherEntity>
    fun fetchCurrentWeather(): Completable
}
