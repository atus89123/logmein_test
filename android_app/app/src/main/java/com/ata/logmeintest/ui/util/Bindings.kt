package com.ata.logmeintest.ui.util

import android.view.View
import android.widget.*
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.bumptech.glide.Glide

object BindingAdapter {

    @JvmStatic
    @BindingAdapter("weatherAbbr")
    fun loadWeatherImage(view: ImageView, imageAbbr: String?) {
        if (!imageAbbr.isNullOrEmpty()) {
            Glide
                .with(view.context)
                .load("https://www.metaweather.com/static/img/weather/png/$imageAbbr.png")
                .centerCrop()
                .into(view)
        }
    }

    @JvmStatic
    @BindingAdapter("isRefreshing")
    fun refreshing(view: SwipeRefreshLayout, isRefreshing: Boolean) {
        view.isRefreshing = isRefreshing
    }
}

object BindingConverters {

    @BindingConversion
    @JvmStatic
    fun booleanToVisibility(isVisible: Boolean): Int = if (isVisible) View.VISIBLE else View.GONE
}