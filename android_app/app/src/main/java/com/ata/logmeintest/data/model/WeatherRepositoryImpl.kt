package com.ata.logmeintest.data.model

import com.ata.logmeintest.*
import com.ata.logmeintest.data.local.weather.WeatherDao
import com.ata.logmeintest.data.local.weather.WeatherEntity
import com.ata.logmeintest.data.remote.WeatherApi
import com.ata.logmeintest.data.remote.dto.WeatherDTO
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.threeten.bp.LocalDateTime
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(
    private val weatherApi: WeatherApi,
    private val weatherDao: WeatherDao
) : WeatherRepository {

    private val weatherAdapter = object {
        val weather: BehaviorSubject<WeatherEntity> = BehaviorSubject.create()
        fun get() = weather
    }

    /**
     * When Room table is empty, then observable events events wont be called,
     * so we have to use single to handle query from empty table.
     *
     * It could be handled with room pre population and the code of this function could be cleaner, more readable.
     */
    override fun getCurrentWeather(): Observable<WeatherEntity> {
        return weatherDao.getWeatherOrEmpty()
            .subscribeOn(Schedulers.io())
            .toObservable()
            .doOnNext {
                weatherAdapter.weather.onNext(it)
            }
            .onErrorResumeNext { _: Throwable ->
                updateCache()
            }
            .flatMap {
                if (it.savedTime.asLocalDateTime().diffInMillis(LocalDateTime.now()) > MINUTE) {
                    updateCache()
                } else {
                    weatherAdapter.get()
                }
            }
    }

    private fun updateCache() =
        fetchCurrentWeather()
            .andThen(weatherDao.getWeather())
            .flatMap {
                weatherAdapter.weather.onNext(it)
                weatherAdapter.get()
            }

    override fun fetchCurrentWeather(): Completable =
        weatherApi.getWeathers()
            .flatMapCompletable {
                weatherDao.saveWeather(it.weathers.first().toWeatherEntity())
            }

    private fun WeatherDTO.toWeatherEntity() =
        WeatherEntity(
            savedTime = LocalDateTime.now().asMillis(),
            name = name,
            imageAbbr = imageAbbr,
            created = isoFormatToMillis(created),
            temp = temp
        )
}