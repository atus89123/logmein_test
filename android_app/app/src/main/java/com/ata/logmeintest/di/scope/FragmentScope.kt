package com.ata.logmeintest.di.scope

import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention
annotation class FragmentScope
