package com.ata.logmeintest.data.remote

import com.ata.logmeintest.data.remote.dto.WeatherDTO
import com.ata.logmeintest.data.remote.dto.Weathers
import com.ata.logmeintest.di.module.AppModule
import org.junit.Test

class WeatherApiTest: BaseApiTest() {

    private fun createSut(url: String): WeatherApi =
        AppModule().run {
            provideWeatherApi(
                url,
                provideObjectMapper(),
                provideOkhttpClient()
            )
        }

    private val getWeather = EndPointTest(
        createSut = ::createSut,
        mockSuccessfulResponse = {
            mockResponse(
                """
                {
                    "consolidated_weather": [
                        {
                            "id": 5548587770445824,
                            "weather_state_name": "Showers",
                            "weather_state_abbr": "s",
                            "wind_direction_compass": "NE",
                            "created": "2021-03-30T10:23:55.848630Z",
                            "applicable_date": "2021-03-30",
                            "min_temp": 10.515,
                            "max_temp": 17.39,
                            "the_temp": 14.485,
                            "wind_speed": 2.003479122550969,
                            "wind_direction": 40.44159225715025,
                            "air_pressure": 1031,
                            "humidity": 67,
                            "visibility": 8.277285651793527,
                            "predictability": 73
                        }
                     ]
                }
                """.trimIndent()
            )
        },
        callEndPoint = WeatherApi::getWeathers,
        expectSuccessfulResponse = {
            expectItems(
                Weathers(
                    weathers = listOf(
                        WeatherDTO(
                            name = "Showers",
                            imageAbbr = "s",
                            temp = 14.485f,
                            created = "2021-03-30T10:23:55.848630Z"
                        )
                    )
                )
            )
        })


    @Test
    fun `Getting user profile succeeds`() = getWeather.testSuccess()
}