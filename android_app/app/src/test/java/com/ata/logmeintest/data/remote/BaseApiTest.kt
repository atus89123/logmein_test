package com.ata.logmeintest.data.remote

import io.reactivex.Observable
import junit.framework.Assert
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before

abstract class BaseApiTest {

    lateinit var mockWebServer: MockWebServer

    @Before
    fun setUpServer() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
    }

    @After
    fun tearDownServer() {
        mockWebServer.shutdown()
    }

    inner class EndPointTest<Response, API>(
        val createSut: (String) -> API,
        val mockSuccessfulResponse: () -> Unit,
        val callEndPoint: API.() -> Observable<Response>,
        val expectSuccessfulResponse: Observable<Response>.() -> Unit
    ) {

        fun withSut(action: API.() -> Unit) {
            createSut(mockWebServer.url("/").toString()).apply(action)
        }

        fun testSuccess() {
            withSut {
                mockSuccessfulResponse()
                callEndPoint().expectSuccessfulResponse()
            }
        }
    }

    fun <T> Observable<T>.expectItems(vararg items: T) {
        Assert.assertEquals(items.toList(), toList().blockingGet())
    }


    fun mockResponse(response: String, responseCode: Int = 200) {
        mockWebServer.enqueue(MockResponse().setResponseCode(responseCode).setBody(response))
    }
}